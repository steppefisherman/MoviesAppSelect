package com.example.moviesappselect.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.moviesappselect.model.ResultApp

class MainFragmentDiffUtil(
    private val oldList: List<ResultApp>,
    private val newList: List<ResultApp>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size
    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].display_title == newList[newItemPosition].display_title
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return when {
            oldList[oldItemPosition] != newList[newItemPosition] -> false
            else -> true
        }
    }
}
